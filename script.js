// getCube Variable
const num = 2;
const getCube = num ** 3;


//Print Template Literals
console.log(`The cube of ${num} is ${getCube} `);

// Adress
let address = [258, "Washington Ave NW", "California", 90011];
let [number, street, state, zipCode] = address;

// Destructure Address
console.log(`I live at ${number} ${street} ${state}, ${zipCode}`);

// Animal
const animal = {
	name: "Lolong",
	typeOfAnimal: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in."
}

const {name, typeOfAnimal, weight, measurement} = animal;

// Destructure Animal
console.log(`${name} was a ${typeOfAnimal}. He weighted at ${weight} with a measurement of ${measurement}`);

// Number Array
let numArray = [1,2,3,4,5];

// Loop Number Array
numArray.forEach((numbers) => {
	return console.log(numbers);
})

// Class of dog
class Dog {
		constructor(name,age,breed) {
			this.name = name;
			this.age = age;
			this.breed = breed;
		}
}

// Instantiate new object
let dachshund = new Dog('Frankie', 5, "Miniature Dachshund");

console.log(dachshund);




